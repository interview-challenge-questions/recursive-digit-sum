package org.kurczynski;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class Solution {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        String outputPath = System.getenv("OUTPUT_PATH");
        BufferedWriter bufferedWriter;

        /* The default HackerRank generated code fails if there's no OUTPUT_PATH env var set */
        if (outputPath == null) {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));
        } else {
            bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        }

        String[] nk = scanner.nextLine().split(" ");

        String n = nk[0];

        int k = Integer.parseInt(nk[1]);

        int result = superDigit(n, k);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }

    private static long sumInput(long value) {
        long sum = 0;

        if (value < 10) {
            return value;
        } else {
            long mod = value % 10;
            long input = (value - mod) / 10;
            sum += sumInput(input) + mod;
        }

        return sum;
    }

    private static long sumInput(String input) {
        long sum = 0;

        for (char i : input.toCharArray()) {
            sum += Integer.parseInt(String.valueOf(i));
        }

        return sum;
    }

    private static long getSuperDigit(long value) {
        long out;

        if (value < 10) {
            return value;
        } else {
            out = getSuperDigit(sumInput(value));
        }

        return out;
    }

    private static int superDigit(String n, int k) {
        long stringSum = sumInput(n);

        /* This number will be less than 10, so casting down to an integer will be fine */
        return (int) getSuperDigit(stringSum * k);
    }
}
