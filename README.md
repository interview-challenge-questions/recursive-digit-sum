# recursive-digit-sum
This is a HackerRank challenge I did recently on 2019-11-08. I've used
HackerRank before, but this time around I formed some different opinions on what
it tries to accomplish that I'll go into detail about below. This is definitely
better suited as a blog post, but I haven't had the time to create a website
recently, so this will have to do for now.


The challenge was called [Recursive Digit Sum](https://www.hackerrank.com/challenges/recursive-digit-sum/problem)
(also [here](./images/challenge.png) in case they move the page). It basically tries
to get you to recursively find the sum of the digits for an enormously large
number that can only be represented as a string because it's too big for an `int`
or a `long` (might have worked with Java's `BigInteger`, but that doesn't seem
like the direction they were going). Seems like a reasonable challenge, let's
give it a whirl.

## Submissions
This repo contains all of the test files used in in these submission for
[input](./input) and [output](./output).

### Attempt #1
After shaping my code to fit in with what HackerRank needs for input and output,
this is what I came up with.

#### Solution.java@3d5dbd58d1227ea924799ff1955bc94d80b368e7
```
package org.kurczynski;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class Solution {
    /* Key-value pair for input string and number of times to repeat it */
    public static class Input {
        String n;
        int k;

        Input(String n, int k) {
            this.n = n;
            this.k = k;
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        String outputPath = System.getenv("OUTPUT_PATH");
        BufferedWriter bufferedWriter;

        /* The default HackerRank generated code fails if there's no OUTPUT_PATH env var set */
        if (outputPath == null) {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));
        } else {
            bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        }

        String[] nk = scanner.nextLine().split(" ");

        String n = nk[0];

        int k = Integer.parseInt(nk[1]);

        int result = superDigit(n, k);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }

    private static int sumInput(int value) {
        int sum = 0;

        if (value < 10) {
            return value;
        } else {
            int mod = value % 10;
            int input = (value - mod) / 10;
            sum += sumInput(input) + mod;
        }

        return sum;
    }

    private static int sumInput(String input) {
        int sum = 0;

        for (char i : input.toCharArray()) {
            sum += Integer.parseInt(String.valueOf(i));
        }

        return sum;
    }

    private static int getSuperDigit(int value) {
        int out;

        if (value < 10) {
            return value;
        } else {
            out = getSuperDigit(sumInput(value));
        }

        return out;
    }


    private static int superDigit(String n, int k) {
        String input = generateInput(new Input(n, k));
        int stringSum = sumInput(input);

        return getSuperDigit(stringSum);
    }

    private static String generateInput(Input input) {
        StringBuilder generated = new StringBuilder();

        for (int i = 0; i < input.k; i++) {
            generated.append(input.n);
        }

        return generated.toString();
    }
}
```

Let's check if the test cases work on my machine:
```
$ java -jar recursive-digit-sum-1.0-SNAPSHOT.jar < ../input/test-case-0
3
$ java -jar recursive-digit-sum-1.0-SNAPSHOT.jar < ../input/test-case-1
8
$ java -jar recursive-digit-sum-1.0-SNAPSHOT.jar < ../input/test-case-2
9
```

Yup! Now let's see what HackerRank thinks for the same test cases:
![](./images/attempt-1-run-code.png)

Cool, so far so good. Let's see what happens when I submit it:
![](./images/attempt-1-submit-code-failures.png)

Uh oh, test cases 6 through 9 seem to have failed due to some kind of runtime
error. But, test cases 0 through 5, 10, and 11 all passed.

So, let's try and figure out what's causing those runtime errors that have
given zero additional context as to what the error actually is. Looks like
I have to use 5 of my "hackos" to see what one of the failed tests cases
are, so let's check out what the input for test case 6 was that caused one
of the runtime errors. [Test case 6](./input/test-case-6) seems to just
a bunch of numbers with an expected output of `3`.

Let's use that test input on my machine to try and get more information
about this runtime error:
```
$ java -jar recursive-digit-sum-1.0-SNAPSHOT.jar < ../input/test-case-6
3
```
Strange, it seems to have worked and even gave the correct answer. I guess
I can try to run the input for test case 6 using the `Test against custom
input` option on HackerRank to see if that gives me any more info:
![](./images/attempt-1-test-case-6-failure-custom-input.png)

`java.lang.OutOfMemoryError`? I never got one of those on my machine, but it
looks like it's happening when the input is being generated with the string
builder. Which means that however HackerRank is running the code, the JVM memory
settings must be a bit lower than what I'm using. That makes sense for
HackerRank to use smaller environments than what I'm using to run each of
these challenges, but it's annoying that those resource limitations weren't
mentioned in the challenge description.

### Attempt #2
To fix the `java.lang.OutOfMemoryError` problem, I need to change how the input
string of numbers is generated so it doesn't use as much memory and will be able
to run on HackerRank. Let's try changing the previous code:
```
    private static int superDigit(String n, int k) {
        String input = generateInput(new Input(n, k));
        int stringSum = sumInput(input);

        return getSuperDigit(stringSum);
    }

    private static String generateInput(Input input) {
        StringBuilder generated = new StringBuilder();

        for (int i = 0; i < input.k; i++) {
            generated.append(input.n);
        }

        return generated.toString();
    }

```

To use less memory:
```
    private static int superDigit(String n, int k) {
        int stringSum = sumInput(n);

        return getSuperDigit(stringSum * k);
    }
```
Instead of creating a giant string of size `n` * `k`, let's just sum `n` and
then multiply that by `k`. This cuts down the memory usage by summing what can
be a very large number `n` and multiplying that by `k`, instead of creating an
even larger number `n` * `k` and then summing that.

#### Solution.java@ed2e922d0f54032a7a73ecc57281ad9d8cf9ae17
After making the changes to how an input string is generated, the code now
looks like this:
```
package org.kurczynski;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class Solution {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        String outputPath = System.getenv("OUTPUT_PATH");
        BufferedWriter bufferedWriter;

        /* The default HackerRank generated code fails if there's no OUTPUT_PATH env var set */
        if (outputPath == null) {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));
        } else {
            bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        }

        String[] nk = scanner.nextLine().split(" ");

        String n = nk[0];

        int k = Integer.parseInt(nk[1]);

        int result = superDigit(n, k);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }

    private static int sumInput(int value) {
        int sum = 0;

        if (value < 10) {
            return value;
        } else {
            int mod = value % 10;
            int input = (value - mod) / 10;
            sum += sumInput(input) + mod;
        }

        return sum;
    }

    private static int sumInput(String input) {
        int sum = 0;

        for (char i : input.toCharArray()) {
            sum += Integer.parseInt(String.valueOf(i));
        }

        return sum;
    }

    private static int getSuperDigit(int value) {
        int out;

        if (value < 10) {
            return value;
        } else {
            out = getSuperDigit(sumInput(value));
        }

        return out;
    }

    private static int superDigit(String n, int k) {
        int stringSum = sumInput(n);

        return getSuperDigit(stringSum * k);
    }
}
```
With those modifications, now let's see how it does with the rest of the tests
when submitted to HackerRank:
![](./images/attempt-2-submit-code-failures.png)

Well, better, but now I have three wrong answers. I'll use 5 more hackos to get
the input of another test case. Looks like test case 9 had a pretty big
[input](./input/test-case-9) and an expected output of `9`. Let's run that on my
machine and see what my wrong answer is:
```
$ java -jar recursive-digit-sum-1.0-SNAPSHOT.jar < ../input/test-case-9
-210513216
```

Ah, there's an `int` overflow somewhere in the summing of the input.

### Attempt #3
To fix the overflow problem, let's change the summing of Java's 32
bit `int` values:
```

    private static int sumInput(int value) {
        int sum = 0;

        if (value < 10) {
            return value;
        } else {
            int mod = value % 10;
            int input = (value - mod) / 10;
            sum += sumInput(input) + mod;
        }

        return sum;
    }

    private static int sumInput(String input) {
        int sum = 0;

        for (char i : input.toCharArray()) {
            sum += Integer.parseInt(String.valueOf(i));
        }

        return sum;
    }

    private static int getSuperDigit(int value) {
        int out;

        if (value < 10) {
            return value;
        } else {
            out = getSuperDigit(sumInput(value));
        }

        return out;
    }

    private static int superDigit(String n, int k) {
        int stringSum = sumInput(n);

        return getSuperDigit(stringSum * k);
    }
```

To use Java's 64 bit `long` values instead:
```
    private static long sumInput(long value) {
        long sum = 0;

        if (value < 10) {
            return value;
        } else {
            long mod = value % 10;
            long input = (value - mod) / 10;
            sum += sumInput(input) + mod;
        }

        return sum;
    }

    private static long sumInput(String input) {
        long sum = 0;

        for (char i : input.toCharArray()) {
            sum += Integer.parseInt(String.valueOf(i));
        }

        return sum;
    }

    private static long getSuperDigit(long value) {
        long out;

        if (value < 10) {
            return value;
        } else {
            out = getSuperDigit(sumInput(value));
        }

        return out;
    }

    private static int superDigit(String n, int k) {
        long stringSum = sumInput(n);

        /* This number will be less than 10, so casting down to an integer will be fine */
        return (int) getSuperDigit(stringSum * k);
    }
```
A 64 bit number _should_ be large enough to represent the sum of each digit
in `n`.

#### Solution.java@3eb42a546d56153d05e4fe3cccefd3a2dd9132a8
Now that the `int` values have been replaced with `long` values,
this is what the code looks like:
```
package org.kurczynski;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class Solution {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        String outputPath = System.getenv("OUTPUT_PATH");
        BufferedWriter bufferedWriter;

        /* The default HackerRank generated code fails if there's no OUTPUT_PATH env var set */
        if (outputPath == null) {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));
        } else {
            bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        }

        String[] nk = scanner.nextLine().split(" ");

        String n = nk[0];

        int k = Integer.parseInt(nk[1]);

        int result = superDigit(n, k);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }

    private static long sumInput(long value) {
        long sum = 0;

        if (value < 10) {
            return value;
        } else {
            long mod = value % 10;
            long input = (value - mod) / 10;
            sum += sumInput(input) + mod;
        }

        return sum;
    }

    private static long sumInput(String input) {
        long sum = 0;

        for (char i : input.toCharArray()) {
            sum += Integer.parseInt(String.valueOf(i));
        }

        return sum;
    }

    private static long getSuperDigit(long value) {
        long out;

        if (value < 10) {
            return value;
        } else {
            out = getSuperDigit(sumInput(value));
        }

        return out;
    }

    private static int superDigit(String n, int k) {
        long stringSum = sumInput(n);

        /* This number will be less than 10, so casting down to an integer will be fine */
        return (int) getSuperDigit(stringSum * k);
    }
}
```

Let's give it a run on my machine:
```
$ java -jar recursive-digit-sum-1.0-SNAPSHOT.jar < ../input/test-case-9
9
```
Awesome, looks like test case 9 is fixed. Time to submit again:
![](./images/attempt-3-success.png)

That did it! All the test cases are now passing.

### Thoughts
Although this challenge did test my ability to write code, it seemed to focus
more on a corner case problem that required consideration of resource
constraints that weren't defined. Because I was never told what the actual
resource limitations were in the environment used to solve the challenge, I
went down a path to figuring out how my environment differs. Even though this
wasn't much of a problem, my ability to figure that out wasn't captured in how
HackerRank identifies a successful challenge.

The problem given in the challenge also felt like an academic exercise with
little relationship to any real-world situation. This is frustrating because
instead of a test of knowledge, my sense of accomplishment came from solving an
inane puzzle.

I think these kind of puzzles can be fun, but they only expose a small fraction
of a person's ability to be a successful software engineer. That's because
theses puzzles only
show if a solution to a problem has been found, it does **not** identify a
person's ability to write reusable code, use a version control system
effectively with meaningful messages, effectively use build automation tools, or
write meaningful unit tests. Those skills, I believe, are often much more
important than a person's ability to solve a puzzle for entertainment.

That being said, it's by no means bad for a person to have the skills to
solve these puzzles effectively. I just don't think it's very useful
to rank a software engineer's ability to solve these kinds of puzzles that
don't include so many other pieces of the software development process.
